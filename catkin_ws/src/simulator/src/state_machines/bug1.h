/********************************************************
 *                                                      *
 *                                                      *
 *      bug1.h			          	        *
 *                                                      *
 *							*
 *		Alberto Embarcadero Ruiz        	*
 *		28-02-2020                              *
 *                                                      *
 ********************************************************/

#define THRESHOLD_FOLLOWER 30
#define THRESHOLD_POINT .01
float dist(float x1,float y1,float x2, float y2){
        return pow(x1-x2,2)+pow(y1-y2,2);
}
float angle(float x1,float y1,float x2, float y2,float theta){
        if (x1-x2==0){
                return -theta;
        }
        return fmod(atan2(y1- y2, x1 - x2)-theta+M_PI,2*M_PI);
}
// State Machine 
int bug1(float intensity, float *light_values, float *observations, int size, float laser_value, int  dest, int obs ,
					movement *movements  ,int *next_state ,float Mag_Advance ,float max_twist,
                                        float robot_x ,float robot_y ,float light_x ,float light_y,float theta, float sensor_value,
                                        float* x_close, float* y_close,float* min_light_distance,int* aux_state,float* x_contact, float* y_contact){


        int state = *next_state;
        int i;

        printf("intensity %f\n",intensity);
        printf("quantized destination %d\n",dest);
        printf("quantized obs %d\n",obs);

        for( i = 0; i < 8; i++)
                printf("light_values[%d] %f\n",i,light_values[i]);
        for ( i = 0; i < size ; i++ ) 
                printf("laser observations[%d] %f\n",i,observations[i]);

        if(intensity > THRESHOLD_FOLLOWER) {

                movements->twist = 0.0;
                movements->advance = 0.0;
                printf("\n **************** Reached light source ******************************\n");
                return 1;
                
        }
        else {
                int next=0,flag=0;
                float light_angle,current_light_distance;
                switch ( state ) {
                        case 0:
                                light_angle=angle(robot_x,robot_y,light_x,light_y,theta);
                                if(light_angle<M_PI){
                                        *movements=generate_output(LEFT,Mag_Advance,light_angle);
                                }else{
                                        *movements=generate_output(RIGHT,Mag_Advance,2*M_PI-light_angle);
                                }
                                *next_state = 1;

                                break;

                        case 1:
                                *movements=generate_output(FORWARD,Mag_Advance,0);
                                next=1;
                                for (int i =(size*2/5);i<(size*3/5)+2;++i){
                                        if(observations[i]<sensor_value){
                                                //guardar punto de contacto y entrar a buscarlo
                                                *aux_state=1;
                                                *x_contact=robot_x;
                                                *y_contact=robot_y;
                                                next = 2;
                                                break;
                                        }
                                }      
                                

                                *next_state=next;
                                break;
                        case 2:
                                *movements=generate_output(LEFT,Mag_Advance,max_twist);
                                next=3;
                                for ( i =(size*3/7);i<(size*4/7);++i){
                                        if(observations[i]<sensor_value){
                                        next = 2;
                                        break;
                                        }
                                }
                                *next_state=next;
                                break;
                        case 3:
                                *movements=generate_output(FORWARD,Mag_Advance,0);
                                next=3;
                                for (i=0;i<size/3;++i){
                                        if(observations[i]>sensor_value/2){
                                                next=4;
                                                break;
                                        }
                                }
                                for(i=(size*2/5);i<(size*3/5);++i){
                                        if(observations[i]<sensor_value*2/3){
                                                next=2;
                                                break;
                                        }
                                }
                                current_light_distance=dist(robot_x,robot_y,light_x,light_y);
                                if(*aux_state==1){//guardar menor distancia hasta que encuentre el punto de contacto
                                        //menor distancia
                                        if(*min_light_distance>current_light_distance){
                                                *min_light_distance=current_light_distance;
                                                *x_close=robot_x;
                                                *y_close=robot_y;
                                        }
                                        //buscar punto de contacto
                                        else if(*x_contact-THRESHOLD_POINT< robot_x && robot_x<(*x_contact)+THRESHOLD_POINT){
                                                if(*y_contact-THRESHOLD_POINT< robot_y && robot_y<(*y_contact)+THRESHOLD_POINT){
                                                        *aux_state=2;
                                                }  
                                        }
                                }else if (*aux_state==2){//buscar punto de menor distancia
                                        if(*x_close-THRESHOLD_POINT< robot_x && robot_x<(*x_close)+THRESHOLD_POINT){
                                                if(*y_close-THRESHOLD_POINT< robot_y && robot_y<(*y_close)+THRESHOLD_POINT){
                                                        *aux_state=0;
                                                        next=0;
                                                }  
                                        }
                                }
                                *next_state=next;
                                break;
                        case 4:
                                next=4;
                                for ( i =1;i<size/3;++i){
                                        if(observations[i]<sensor_value*5/6){
                                        next = 3;
                                        break;
                                        }
                                }
                                
                               

                                *movements=generate_output(RIGHT,Mag_Advance,max_twist/2);
                                
                                *next_state=next;
                                break;
                        
                        default:
                                //printf("State %d not defined used ", state);
                                *movements=generate_output(STOP,Mag_Advance,max_twist);
                                *next_state = 0;
                                break;
                }
                        
        }

        printf("Next State: %d\n", *next_state);
        printf("Min x:%f    y:%f    dist:%f\n",*x_close,*y_close,*min_light_distance);
        printf("Contact x:%f    y:%f    aux:%d\n",*x_contact,*y_contact,*aux_state);
        printf("Robot x:%f    y:%f\n",robot_x,robot_y);
        


}



                 
